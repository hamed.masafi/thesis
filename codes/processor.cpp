#include <QThreadPool>

#include "processor.h"

#ifdef NEURON_CLIENT
#include "converter.h"
#include "imagematcher.h"
#include "imagematcherpool.h"
#endif

N_CLASS_IMPL(Processor)
{
    setIsBusy(false);
}

N_REMOTE_METHOD_IMPL(Processor, showImage, QString, 
                     fileName, int, time)
{
    emit imageFound(fileName, time);
}
N_REMOTE_METHOD_IMPL(Processor, showMessage, 
                     QString, message)
{
    emit messageRecived(message);
}

N_REMOTE_METHOD_IMPL(Processor, search, QImage, image)
{
    emit newImage(image);
#ifdef NEURON_CLIENT
    this->image = image;
#endif
}

N_REMOTE_METHOD_IMPL(Processor, finished, int, id, 
                     int, psnr)
{
    emit resultReturned(id, psnr);
}

N_REMOTE_METHOD_IMPL(Processor, setList, QVariantList, list)
{
#ifdef NEURON_CLIENT
    ImageMatcherPool *pool = new ImageMatcherPool;
    pool->setImage(image);
    QList<int> ids;
    Converter::convert(list, ids);
    pool->setCount(ids.count());
    connect(pool, &ImageMatcherPool::finished,
            this, &Processor::finished);
    foreach (int id, ids)
        pool->run(id);
#else
    Q_UNUSED(list);
#endif
}

N_PROPERTY_IMPL(Processor, int, workersCount, workersCount, 
                setWorkersCount, workersCountChanged)
N_PROPERTY_IMPL(Processor, int, progressValue, 
                progressValue, setProgressValue, 
                progressValueChanged)
N_PROPERTY_IMPL(Processor, int, progressMax, progressMax, 
                setProgressMax, progressMaxChanged)
N_PROPERTY_IMPL(Processor, bool, isBusy, isBusy, setIsBusy, 
                isBusyChanged)
