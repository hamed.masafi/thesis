
%%
%%  This is file `Tabriz_thesis.cls',
%%  The original file was xepersian-thesis.cls by Vafa Khalighi
%%   __________________________________
%%   Copyright © 2011-2012 Vahid Damanafshan
%%  
%%   http://www.damanafshan.ir
%%   http://www.parsilatex.com  
%%   http://forum.parsilatex.com
%% 
%% 
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Shahriar_thesis}
              [08/04/2012 v0.4
 Persian thesis document class in XeLaTeX for University of Tabriz projects/theses/dissertations]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions
\LoadClass[a4paper,11pt]{book}
\def\university#1{\gdef\@university{#1}}
\def\department#1{\gdef\@department{#1}}
\def\degree#1{\gdef\@degree{#1}}
\def\thesisdate#1{\gdef\@thesisdate{#1}}
\def\thesissession#1{\gdef\@thesissession{#1}}
\newcommand{\firstsupervisor}[1]{\def\@firstsupervisor{#1}}
\newcommand{\secondsupervisor}[1]{\def\@secondsupervisor{#1}}
\newcommand{\firstadvisor}[1]{\def\@firstadvisor{#1}}
\newcommand{\secondadvisor}[1]{\def\@secondadvisor{#1}}
\newcommand{\print}{\def\printmode{}}
\def\name#1{\gdef\@name{#1}}
\def\surname#1{\gdef\@surname{#1}}
\def\studentnumber#1{\gdef\@studentnumber{#1}}
\def\keywords#1{\gdef\@keywords{#1}}
\def\fa-abstract#1{\gdef\@fa-abstract{#1}}
\def\faculty#1{\gdef\@faculty{#1}}
\def\subject#1{\gdef\@subject{#1}}
\def\field#1{\gdef\@field{#1}}
\def\department#1{\gdef\@department{#1}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\latintitle#1{\gdef\@latintitle{#1}}
\def\latinauthor#1{\gdef\@latinauthor{#1}}
\def\latindegree#1{\gdef\@latindegree{#1}}
\def\latinfaculty#1{\gdef\@latinfaculty{#1}}
\def\latinthesisdate#1{\gdef\@latinthesisdate{#1}}
\def\latinsubject#1{\gdef\@latinsubject{#1}}
\def\latinfield#1{\gdef\@latinfield{#1}}
\def\firstlatinsupervisor#1{\gdef\@firstlatinsupervisor{#1}}
\def\secondlatinsupervisor#1{\gdef\@secondlatinsupervisor{#1}}
\def\firstlatinadvisor#1{\gdef\@firstlatinadvisor{#1}}
\def\secondlatinadvisor#1{\gdef\@secondlatinadvisor{#1}}
\def\latinname#1{\gdef\@latinname{#1}}
\def\latinsurname#1{\gdef\@latinsurname{#1}}
\def\en-abstract#1{\gdef\@en-abstract{#1}}
\def\latinkeywords#1{\gdef\@latinkeywords{#1}}
\def\latinuniversity#1{\gdef\@latinuniversity{#1}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newif\if@bscthesis
\@bscthesisfalse
\DeclareOption{bsc}{\@bscthesistrue}
\newif\if@mscthesis
\@mscthesisfalse
\DeclareOption{msc}{\@mscthesistrue}
\ProcessOptions
%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main title creation     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\vtitle}{\begin{titlepage}
	%\topmargin=-30pt
	\vspace{-2cm}\centerline{{\includegraphics[height=4.3cm]{images/logo}}}
	\vspace{-.2cm}
	{\large
		\vskip .4cm
		\textbf{\@university}\par
		
		\vspace{-.4cm}گروه آموزشی \@faculty
	}


	% \vspace{-.4cm}\@department}
	\vspace{.6cm}
	{
		\large\bfseries
		%\vskip .5cm
		\if@mscthesis
		پایان‌نامه
		\else 
			\if@bscthesis
		پروژه
			\else
		رساله
			\fi
		\fi
		\writtenfor
		\if@mscthesis
		کارشناسی ارشد
		\else
			\if@bscthesis
			کارشناسی
			\else
			دکتری
			\fi
		\fi
		
		\vskip .2cm
		\centerline{
			\large \bfseries
			در رشته
			\@subject%
			\if@mscthesis%
			گرایش
			\else 
				\if@bscthesis%
				\relax%
				\else%
				گرایش
				\fi
			\fi
			\@field
		}
	}
	\par
	
	\vskip .2cm
	{\huge\bfseries {\baselineskip=1cm \@title}}\baselineskip=1.5cm\par

	
	\vskip .8cm
	{\Large\bfseries   \@name\ \ \@surname}\par
	
	\vskip .8cm

	\large
	{
		\ifx\@firstsupervisor\undefined%
			\ifx\@secondsupervisor\undefined%
			\else
			\fi
		\else \ifx\@secondsupervisor\undefined%
			استاد راهنما
			{
				\Large\bfseries\par
				\@firstsupervisor
			}
			\else
				\large
			استادان راهنما
				\par
				{\Large\bfseries \@firstsupervisor{} و \@secondsupervisor}
			\fi
		\fi
	}

	\vskip .8cm
	\large
	{
		\ifx\@firstsupervisor\undefined%
		\ifx\@secondsupervisor\undefined%
		\else
		\fi
		\else \ifx\@secondsupervisor\undefined%
			استاد مشاور
			{
				\Large\bfseries\par
				\@firstadvisor
			}
		\else
			\large
			اساتید مشاور
				\par
				{\Large\bfseries \@firstadvisor{} و \@secondadvisor}
			\fi
		\fi
	}

	{\large \@thesisdate}
	%\vfill
	
	\end{titlepage}%
}

\newcommand{\vtitleb}{
\begin{titlepage}
	\vspace{-2cm}\centerline{{\includegraphics[height=4.3cm]{images/logo}}}
	\vspace{-.2cm}
	
	{\large گروه \@subject} \\
	{\large گرایش \@faculty}
	\vspace{2cm}
	\\
	
	\vskip .2cm
	{\huge\bfseries {\baselineskip=1cm \@title}}\baselineskip=1.5cm\par
	
	
	\vskip .8cm
	{\large\bfseries   \@name\ \ \@surname}\par
	
	\vskip .8cm
	\large
	{
		\ifx\@firstsupervisor\undefined%
			\ifx\@secondsupervisor\undefined%
			\else
			\fi
			\else \ifx\@secondsupervisor\undefined%
			استاد راهنما
				{
					\Large\bfseries\par
					\@firstsupervisor
				}
			\else
				\large
				اساتید راهنما
				\par
				{\Large\bfseries \@firstsupervisor{} و \@secondsupervisor}
			\fi
		\fi
	}

	\vskip .8cm
	\large
	{
		\ifx\@firstsupervisor\undefined%
		\ifx\@secondsupervisor\undefined%
		\else
		\fi
		\else \ifx\@secondsupervisor\undefined%
		استاد مشاور
			{
				\Large\bfseries\par
				\@firstadvisor
			}
		\else
			\large
				اساتید مشاور
				\par
				{\Large\bfseries \@firstadvisor{} و \@secondadvisor}
			\fi
		\fi
	}
	
	
	{\large \@thesisdate}
\end{titlepage}%
}

\newcommand{\latinvtitle}{%
	\if@bscthesis \relax
	\else
	\begin{titlepage}
		\vspace{-1.5cm}{\includegraphics[height=4.3cm]{images/logo}}
		\centerline{\bf\@latinuniversity} 
		\large\bfseries
		A thesis submitted in Partial fulfillment of the requirements \\
		for the degree of Master of Science
		\vskip 5mm
		
		{\titlefont \@latintitle}
		
		{
			\large %\supervisorfont
			\rm\@latinname \; \@latinsurname
		}\par
		
		\vskip 1cm
		{
			\ifx\@firstlatinsupervisor\undefined%
			\ifx\@secondlatinsupervisor\undefined%
			\else
			\fi
			\else
			\ifx\@secondlatinsupervisor\undefined%
			Supervisor
			{\Large\bfseries\par \@firstlatinsupervisor}
			\else
			\large
			Supervisors
			\par
			{\Large\bfseries \@firstlatinsupervisor{} and \@secondlatinsupervisor}
			\fi
			\fi
		}
		\par
		\large
		\vskip 1cm
		{
			\ifx\@firstlatinadvisor\undefined%
			\ifx\@secondlatinadvisor\undefined%
			\else
			\fi
			\else
			\ifx\@secondlatinadvisor\undefined%
			Advisor
			{\Large\bfseries\par \@firstlatinadvisor}
			\else
			\large
			Advisors
			\par
			{\Large\bfseries\@firstlatinadvisor{} and \@secondlatinadvisor}
			\fi
			\fi
		}
		
		{\@latinthesisdate}
		
		%
	\end{titlepage}
	\begin{titlepage}
		\vspace{-1.5cm}{\includegraphics[height=4.3cm]{images/logo}}
		\centerline{\bf\@latinuniversity} 
		\large\bfseries
		Department of		\@latinfaculty\space 
		\\
		Specialisation: \@latinsubject
		\vskip 5mm
		
		{\titlefont \@latintitle}
		
		{
			\large %\supervisorfont
			\rm\@latinname \; \@latinsurname
		}\par
		
		\vskip 1cm
		{
			\ifx\@firstlatinsupervisor\undefined%
			\ifx\@secondlatinsupervisor\undefined%
			\else
			\fi
			\else
			\ifx\@secondlatinsupervisor\undefined%
			Supervisor
			{\Large\bfseries\par \@firstlatinsupervisor}
			\else
			\large
			Supervisors
			\par
			{\Large\bfseries \@firstlatinsupervisor{} and \@secondlatinsupervisor}
			\fi
			\fi
		}
		\par
		\large
		\vskip 1cm
		{
			\ifx\@firstlatinadvisor\undefined%
			\ifx\@secondlatinadvisor\undefined%
			\else
			\fi
			\else
			\ifx\@secondlatinadvisor\undefined%
			Advisor
			{\Large\bfseries\par \@firstlatinadvisor}
			\else
			\large
			Advisors
			\par
			{\Large\bfseries\@firstlatinadvisor{} and \@secondlatinadvisor}
			\fi
			\fi
		}
		
		{\@latinthesisdate}
		
		%
	\end{titlepage}
	\fi
}

\def\abstractpage{
	\newpage
	\thispagestyle{empty}
	\vspace*{2cm}
	{\Huge\bfseries{چکیده}}
	\vspace*{1cm}
	\phantomsection
	\addcontentsline{toc}{chapter}{چکیده}

	\@fa-abstract
	
	\vspace*{1cm}
	
	\textbf{کلمات کلیدی:} \@keywords
}
\def\latinabstractpage{
	\begin{latin}
		\newpage
		\thispagestyle{empty}
		\vspace*{2cm}
		{\Huge\bfseries{Abstract}}
		\vspace*{1cm}
		\phantomsection
		\addcontentsline{toc}{chapter}{چکیده انگلیسی}
			
		\@en-abstract
		
		\vspace*{1cm}
		
		\textbf{Keywords:} \@latinkeywords
	\end{latin}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Configurations      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\font\titlefont=cmssbx10 scaled 2074
\font\supervisorfont=cmbxti10


\def\onvan{عنوان}
\def\writtenfor{برای دریافت درجه }
\def\by{پژوهشگر}
\def\latinby{by}

\def\signature{
	\vspace{1cm}
	\begin{flushleft}
		{
			\scriptsize \@name\ \@surname \\
			\@thesissession
		}
	\end{flushleft}
}
\let\cleardoublepage\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\acknowledgementpage{\newpage
	\thispagestyle{empty}
	
	\vspace{1cm}
	\par\noindent}
\def\endacknowledgementpage{\newpage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\frame{\newpage
\thispagestyle{empty}
\begin{framed}\thispagestyle{empty}}
\def\endframe{\thispagestyle{empty}\end{framed}
\thispagestyle{empty}
\newpage
}

\def\titlepage{\newpage\centering
  \thispagestyle{empty}
  \parindent 0pt \parskip 10pt plus 1fil minus 1fil
  \def\baselinestretch{1}\@normalsize\vbox to \vsize\bgroup\vbox to 9in\bgroup}
\def\endtitlepage{\par\kern 0pt\egroup\vss\egroup\newpage}



\def\abstract{\subsection*{\abstractname}\small
\@normalsize}
\def\endabstract{\par}

